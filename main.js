answers = ["A","C","B"];
$(document).ready(function(){
    $("label").click(function(){
        if(!$(this).hasClass("blocked")){
            rname=$(this).attr("for");
            $("label[for="+rname+"]").addClass("blocked");
            $("label[for="+rname+"] > input").attr("checked",false);
            $(this).children("input").attr("checked",true);
            if( $(this).children("input").attr("value")==answers[(rname.substr(1, 1))-1] ){
                $(this).addClass("green");
            }
            else{
                $(this).addClass("red");
            }
            $(this).children("input").attr("checked",true);
        }
    });
});